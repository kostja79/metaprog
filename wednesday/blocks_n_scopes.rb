# contrary to java, there is no outer scope, that is transferred to the inner scopes
v1 = 1
class MyClass # SCOPE GATE: entering class

  v2 = 2
  p local_variables # => [:v2]

  def my_method # SCOPE GATE: entering def

    v3 = 3
    p "my_method: #{local_variables}"
  end # SCOPE GATE: leaving def

  p local_variables
end # SCOPE GATE: leaving class

obj = MyClass.new
obj.my_method
obj.my_method
p local_variables
