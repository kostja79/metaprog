def double_it(callable)
  p callable.call*2
end

# the right way - avoid explicit returns from procs:
p = Proc.new { 10 }
double_it(p)

# and now exploring the wrong ways

def another_double
  p = Proc.new { return 10 }
  result = p.call
  return result * 2 # this line is unreachable code!
end

p another_double


# here, everything works fine, the lambda returns as expected
double_it(lambda {return 10})

# there is also another option for defining lambdas since 1.9 - 'stabby lambda', or 'lambda literal'
double_it(->(){return 10})

# unexpected return (LocalJumpError), as the proc tries to return from the context where it was defined - the main
double_it(proc{return 10})









