require 'rspec'

def event(name, &block)

  #create a clean room object for execution
  cleanRoom = BasicObject.new
  @setups.each { |setup| cleanRoom.instance_eval(&setup) } if @setups
  "ALERT #{name}" if cleanRoom.instance_eval(&block)
end

def setup(&set)
  @setups ||= []
  @setups << set
end


describe 'event and setup' do

  it 'should raise an ArgumentError if no block is given' do
    lambda { event(:bob) }.should raise_error(ArgumentError)
  end

  it 'should return nil if given block yields false' do
    event(:bob) { false }.should be_nil
  end
  it 'should return an ALERT string if given block yields true' do
    event(:bob) { true }.should =~ /ALERT bob/
  end

  it 'should store the setup block' do

    @setups.should be_nil
    setup do
      'setting up'
    end
    @setups.should_not be_nil
    @setups.should_not be_empty
    @setups.size.should be 1
    event(:bob) { true }.should =~/ALERT bob/
  end

  it 'should store and execute multiple setups' do
    @setups.should be_nil
    setup do
      'setting up'
    end
    setup do
      'setting up again'
    end
    @setups.should_not be_nil
    @setups.should_not be_empty
    @setups.size.should be 2
    event(:bob) { true }.should =~/ALERT bob/
  end

  it 'should store and execute setups or multiple events' do
    @setups.should be_nil
    setup do
      'setting up'
    end
    @setups.should_not be_nil
    @setups.should_not be_empty
    @setups.size.should be 1
    # how to test that the setup procs are actually called without relying on the console output
    event(:bob) { true }.should =~/ALERT bob/
    event(:jim) { true }.should =~/ALERT jim/
  end
end