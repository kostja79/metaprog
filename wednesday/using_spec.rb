require 'rspec'

class Resource

  def dispose
    @disposed = true
  end

  def disposed?
    @disposed
  end

end

class Object
  def using(resource)
    begin
      p 'beginning'
      yield if block_given?
    ensure
      p 'after yield'
      resource.dispose
    end
  end
end


  describe Resource do

    let(:resource) { Resource.new }

    it 'should be disposed from the start' do
      resource.disposed?.should be_false
    end

    it 'should dispose argument after block' do
      using(resource) { "do stuff" }
      resource.disposed?.should be_true
    end

    it 'should execute block' do
      expect { using(resource) { raise Exception } }.to raise_exception
      resource.disposed?.should be_true
    end
  end