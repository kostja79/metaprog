# so whats about Kernel#method ?

# A method is like a lambda, with the difference that it
# is executed inside the context of the defining object
class MyClass

  def initialize(value)
    @var = value
  end

  def as_method
    p 'defined as method'
    p "var: #{@var}"
  end
end


object = MyClass.new(1)
asm = object.method :as_method
p asm.class # Method
p asm.arity # 0
p asm.owner # Object

asm.call

# unbinding
unbound = asm.unbind
p "owner: #{unbound.owner}" # even if it is unbound, the owner class remains
another_object = MyClass.new(2)

#binding to another object, to give execution context
asm = unbound.bind(another_object)
asm.call


# metod to proc
p asm.to_proc

# block to method
object.class.send(:define_method, :from_block) { puts 'I am just a method from the block'}
p "finding the method we defined a line earlier: #{object.methods.grep(/from_block/)}"
object.from_block


# you can only bind a method to an object of the same type as the original object
# asm.unbind.bind(Object.new) # bind argument must be an instance of MyClass (TypeError)


# you cannot call unbound methods:
# undefined method `call' for #<UnboundMethod: MyClass#as_method> (NoMethodError)
asm.unbind.call




