def math(a, b)
  yield(a, b) # yields to the passed block, passing the params
end

# the & operator transforms the passed block into a Proc
def teach_math(a, b, &operation)

  p operation.class
  p "Let's do the math:"

  p math(a, b, &operation)
end

def teach_math2(a, b, operation)

  p operation.class
  p "Let's do some more math:"

  # here we transform the Proc to a block again
  p math(a, b, &operation)
end

teach_math(2, 3) { |x, y| x * y }

teach_math2(2, 3, proc { |x, y| x*y*10 })

teach_math2(2, 3, lambda { |x, y| x*y*10 }) # also works with lambdas

# transforming a proc to a block again

def my_method(greeting)# expacts a block, not a proc
  p "#{greeting}, #{yield}!"
end

my_proc = proc { 'Bill' }
my_method('Hello', &my_proc)
