
# using Proc#new
proc1 = Proc.new {|x| p x + 100}

#using Kernel#proc
proc2 = proc {|x| p x + 100}

#using Kernel#lambda
lam1 = lambda {|x| p x + 100}


# Deferred Evaluation

proc1.call(1)
proc2.call(2)
lam1.call(3)

# all the 3 possibilities create a Proc instance

p proc1.class
p proc2.class
p lam1.class


