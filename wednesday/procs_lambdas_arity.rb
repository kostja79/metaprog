# procs are very permissive WRT arity, dropping excessive args and replacing the missing ones by nil

pr = Proc.new { |a, b| [a, b] }
p pr.arity # 2

p pr.call(1, 2, 3) # [1, 2] - excessive args are simply dropped
p pr.call(1) # [1] - missing args are simply dropped

# lambdas are strict WRT arity, raising exceptions on wrong args

la = lambda { |a, b| [a, b] }

# p la.call(1, 2, 3) # wrong number of arguments (3 for 2) (ArgumentError)
p la.call(1) # wrong number of arguments (1 for 2) (ArgumentError)


