# ---
# Flat Scope (nested lexical scopes)

# this var should be accessible inside the class and from there inside the instance var
my_var = 'Success'

MyClass = Class.new do

  p "#{my_var} in the class definition!"

  define_method :my_method do
    p "#{my_var} in the method!"

  end
end

MyClass.new.my_method

#---
# Shared Scope
p 'shared scope'

def my_scope

  shared_var = 'Hi'

  # here we need dynamic dispatch to access define_method as a private method of Kernel
  # when inside a class, we use the public Module#define_method

  Kernel::send :define_method, :get_shared do
    p "first method: #{shared_var}"
  end

  Kernel::send :define_method, :change_shared do
    shared_var = 'Bye'
    p "second method: #{shared_var}"
  end

end

my_scope # needs to be called to run the definition

get_shared
change_shared

