class MyClass
  def initialize
    @a = 123
  end
end

p self

a = MyClass.new

# the block paswsed to instance_eval is called a 'Context Probe'
a.instance_eval do
  p self
  p @a
end

b = 321
# the instance variable is assigned to the value of the variable in the 'flat' scope
a.instance_eval {@a = b}
a.instance_eval {p @a}

# 'instance_eval' does not accept params, so ruby 1.9 introduces 'instance_exec'

# a.instance_eval(3) {|x| p @a + x} # raises ArgumentError

a.instance_exec(333) {|x| p @a + x}







