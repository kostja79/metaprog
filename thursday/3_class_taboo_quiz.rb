class MyClass < Array
  def my_method
    p 'Hello!'
  end
end

MyClass.new.my_method

c = Class.new(Array)
c.class_eval do
  def my_method
    p 'Hi!'
  end
end

MyClass = c # issues a warning: already initialized constant MyClass

MyClass.new.my_method

c = Class.new(Array) do
  def my_method
    p 'Bye!'
  end
end

MyClass = c # issues a warning: already initialized constant MyClass

MyClass.new.my_method
