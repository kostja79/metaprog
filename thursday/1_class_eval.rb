require 'rspec'

# class_eval effectively reopens the class

def add_method_to_class(a_class)
  a_class.class_eval do
    def m
      p 'Im a new method'
    end
  end
end


describe 'adding method to a class' do

  let(:myclass) {
    class MyClass
      self
    end }

  it 'should not have a method :m' do
    myclass.respond_to?(:m).should be_false
  end

  it 'should add a method :m' do
    add_method_to_class(myclass)
    # since it's a new instance method, we need to use myclass.new
    myclass.new.respond_to?(:m).should be_true
  end

end

