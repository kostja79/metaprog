# enhancing a specific obejct on the fly

str = 'some string'

p str.singleton_methods #=> []

def str.my_method # defining a singleton method on the str object
  p reverse
end

p str.singleton_methods #=> [:my_method]

p 'other string'.singleton_methods #=> []

str.my_method # => gnirts emos


# so class methods are in reality singleton methods of a class

