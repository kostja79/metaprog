# class vars d(o not belong to single classes, they belong to the whole class hierarchies

@@v = 1

class MyClass
  @@v = 2
end

# since @@v belongs to all subclasses of Object (the class of main), it was redefined inside the MyClass definition
p @@v # => 2


# more explicit:

class C
  @@v = 1
end

class D < C
  def my_method;
    p @@v;
  end
end
D.new.my_method # => 1
