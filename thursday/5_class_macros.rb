# suppose the methods of a class are used by clients.
# However, you would like to deprecate those methods in a non-invasive fashion

# for this, class methods can be used, that look like language keywords, but are methods really

class MyClass

  def new_method
    p 'hi, Im new here'
  end

  # we want to use the deprecate macro on the descendants of MyClass only
  # to use the macro on all classes, reopen the Class and add an instance method:

  # class Class
  #    def new_macro end
  # end

  def self.deprecate(old_name, new_name)
    define_method(old_name) do |*args, &block|
      warn " method #{old_name}() is deprecated, please use #{new_name}() instead"
      send(new_name, *args, &block)
    end
  end

  deprecate :old_method, :new_method
end

MyClass.new.old_method