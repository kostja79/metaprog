# in order to use :attr_accessor on a class and not an instance, we could open Class

class Class
  attr_accessor :a
end

Class.a = 10 # it works
p Class.a

# but now, all classes have the :a attr

p Integer.a # nil - the attribute is there, but not the value, since Integer is an instance of Class


# so a better way is to use the eigenclass scope, so only a single class has the attribute
class MyClass
  class << self
    attr_accessor :b
  end
end

MyClass.b = 3
p MyClass.b


