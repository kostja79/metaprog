# where do singleton methods (eigenmethods) live? They do not live in their respective objects,
# since those are in general not classes...

# each object can have it's own special and hidden class specially for this purpose - it's called an 'eigenclass'
# also called singleton classes, or metaclasses
obj = Object.new

eigenclass = class << obj # this syntax opens the scope of the eigenclass
  self # => #<Class:#<Object:0x00000...>>
end

class << obj
  def eigenclass_scoped_method
    p 'this method is defined using the "class <<" syntax'
  end
end

def obj.my_singleton_method
  p 'this method is defined using the "def obj.method" syntax'
end

obj.eigenclass_scoped_method
obj.my_singleton_method

p obj.singleton_methods
p eigenclass.instance_methods.grep(/my_sing/)


# defining an eigenmethod with instance_eval

var = 'abc' # this object gets the singleton method
var2 = 'bcd' # this doesn't

var.instance_eval do
  def my_eigenmethod
    p 'this method is defined using the "instance_eval" syntax'
    p self.reverse
  end
end

var.my_eigenmethod # => 'cba'
p var2.respond_to? :my_eigenmethod # => false


