class MyClass

  def old_method
    p 'my_method'
  end

  alias :new_method :old_method # alias is a keyword, not a method, so no ',' is needed between the operands
end

obj = MyClass.new
obj.old_method # 'my_method'
obj.new_method # 'my_method'

class MyClass
  alias_method :another_new_method, :old_method # Kernel#alias_method needs ','
end

obj.another_new_method # 'my_method'


# defining an 'around alias' for the :length method
class String
  alias :real_length :length

  # we are redefining the length method here, but the alias still points to the original method
  def length
    real_length > 5 ? 'long' : 'short'
  end
end

abc = 'abc'
p abc.length # 'short'
p abc.real_length # 3

# a method redefinition does not change the method, is simply assigns the name to a different method
# all variables/aliases pointing to the original methods will continue working with it