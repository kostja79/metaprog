# opening the Object to add a method for returning the otherwise hidden eigenclass
class Object
  def eigenclass
    class << self
      self
    end
  end
end

class A
  def a_method
    p 'A#a_method'
  end

  # due to inheritance, this method will be available to deriving classes
  # it is then defined in the eigenclass of teh superclass
  def self.a_class_method
    p 'A#a_class_method'
  end
end

class B < A
end

p B.a_class_method # => A#a_class_method

obj = B.new
obj.a_method # => A#a_method

class << obj
  def an_eigenmethod
    p 'obj#an_eigenmethod'
  end
end

obj.an_eigenmethod

p obj.eigenclass #=> #<Class:#<B:0x000...>>
p obj.eigenclass.class #=> Class - no surprises here
p obj.eigenclass.superclass # => B

p A.eigenclass # => #<Class:A>
p B.eigenclass # => #<Class:B>

# the superclass of the eigenclass is the eigenclass of the superclass

p B.eigenclass.superclass #<Class:A>
p A.eigenclass.superclass #<Class:Object>

