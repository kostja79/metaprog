# inheriting a Module's eigenmethods does not work outright - they are in it's eigenclass,
# which isn't part of the class hierarchy

module MyModule
  def self.module_method
    p 'MyModule#module_method'
  end
end

p MyModule.singleton_methods #=> [:module_method]

class MyClass
  include MyModule
end

# the class does not inherit the singleton methods of the module
p MyClass.singleton_methods # => []

###  - the proposed solution is to define the method in the module as a regular instance method
### and then include the module in the eigenclass of the class
### this is called 'Class Extension'
module MyModule
  def inst_method
    p 'MyModule#inst_method'
  end
end

class MyClass
  class << self
    include MyModule
  end
end

p MyClass.singleton_methods # => [:inst_method]

