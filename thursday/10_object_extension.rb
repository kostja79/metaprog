### the general technique of adding singleton methods by including modules into the eigenclass
### Object Extension

module MyModule
  def inst_method
    p 'MyModule#inst_method'
  end
end

obj = Object.new

p obj.singleton_methods # []

class << obj # Object extension syntax 1
  include MyModule
end

p obj.singleton_methods # [:inst_method]

obj2 = Object.new

p obj2.singleton_methods # []

obj2.extend MyModule # Object extension syntax 2
p obj2.singleton_methods # [:inst_method]