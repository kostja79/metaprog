require 'rspec'

module Kernel
  def add_checked_attr(klass, name, &validation)
    klass.class_eval do

      define_method "#{name}=" do |value|
        raise ArgumentError unless validation.call(value)
        instance_variable_set("@#{name}", value)
      end

      define_method "#{name}" do
        instance_variable_get "@#{name}"
      end
    end
  end
end

class Person
end

describe Person do

  add_checked_attr(Person, :age) do |age|
    age > 17
  end

  let(:bob) { Person.new }

  it 'should add accessor methods' do
    bob.should respond_to :age
    bob.should respond_to :age=
  end

  it 'should accept valid values' do
    bob.age = 20
    bob.age.should be 20
  end

  it 'should reject invalid values' do
    lambda { bob.age = 17 }.should raise_error(ArgumentError)
  end
end
