class MyClass

  attr_accessor :a

  def getBinding
    binding
  end
end

obj = MyClass.new
obj.a = 10

b = obj.send(:binding) # does not work - why?
p b
p eval('@a', b) # nil

b = obj.getBinding
p eval('@a', b) # 10

# ---

p eval('@a/0', b) # here, the error is returned with a proper file and linenum
                  # but in general it is advisable to provide those when calling eval
                  # http://olabini.com/blog/2008/01/ruby-antipattern-using-eval-without-positioning-information/





