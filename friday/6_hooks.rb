class String

  # :inherited is a hook method with no default impl
  def self.inherited(subclass)
    p "#{self} has been inherited by #{subclass}"
  end
end

class MyString < String;
end;

# same goes for module inclusion - :included
module MyModule
  def self.included(other)
    p "#{self} has been mixed into #{other}"
  end
end

class C
  include MyModule
end

# :extend_object
module MySingletonMethods
  def my_singleton_method
    p "#{self}#my_singleton_method"
  end

  def self.extend_object(obj)
    p "#{self} has been extended by #{obj}"
  end
end

obj = Object.new

# this syntax does not trigger the extend_object hook, but the singleton methods are there
class << obj
  include MySingletonMethods
end

p obj.singleton_methods.grep(/my_/)

# this syntax 'does' trigger the hook
obj.extend MySingletonMethods

# :method_added, :method_removed, :method_undefined

class A
  def self.method_added(name)
    p "instance method #{name} has been added to #{self}"
  end
end

# both method definition syntaxes trigger the hook
class A
  def some_method
    p 'doin nothin'
  end

  define_method :other_method do
    p 'not doin anythin'
  end
end
