# a mixin defining class methods on it's inclusor, p.185

module MyClassMixin

  def self.included(inclusor)
    p "#{self} has been included by #{inclusor}"
    inclusor.extend(ClassMethods) # mixing ClassMethods in
  end

  # this method will be a class method of the including class
  # we are encapsulating the methods in a separate module
  # without this additional module, we would have the method both
  # as class and as instance methods
  module ClassMethods
    def my_class_method
      p "#{self}#my_class_method"
    end
  end
end

class MyClass
  include MyClassMixin
end

p MyClass.methods.grep(/my_class_method/) # [:my_class_method]
p MyClass.instance_methods.grep(/my_class_method/) # []
