def eval_exploit(arg)
  p "evaluating #{arg}"
  p eval arg
end

def eval_untaint(arg)
  p "evaluating #{arg}"
  arg.untaint # <--- untainting the dangerous input before eval
  p eval arg
end

p "safe level: #{$SAFE}"
eval_exploit(gets())

$SAFE = 1

p "safe level: #{$SAFE}"
eval_untaint(gets())

# eval_exploit(gets()) # Insecure operation - eval (SecurityError)
