require 'rspec'

# contrary to the text-based eval in the first part of the quiz, this 'does' work if this method is added to the reopened Kernel module
module Kernel
  def add_checked_attr(klass, name)
    klass.class_eval do

      define_method "#{name}=" do |value|
        raise ArgumentError unless value
        instance_variable_set("@#{name}", value)
      end

      define_method "#{name}" do
        instance_variable_get "@#{name}"
      end
    end
  end
end

# in the first attempt, checked values accept everything except nil and false
class Person
end

describe Person do

  let(:bob) { Person.new }

  it 'should add accessor methods' do
    bob.should_not respond_to :age
    bob.should_not respond_to :age=

    add_checked_attr(Person, :age)

    bob.should respond_to :age
    bob.should respond_to :age=
  end

  it 'should accept valid values' do

    add_checked_attr(Person, :age)
    bob.age = 20
    bob.age.should be 20
  end

  it 'should reject nil values' do

    add_checked_attr(Person, :age)
    lambda { bob.age = nil }.should raise_error(ArgumentError)
  end

  it 'should reject false values' do
    add_checked_attr(Person, :age)
    lambda { bob.age = false }.should raise_error(ArgumentError)
  end
end
