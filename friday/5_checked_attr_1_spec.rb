require 'rspec'

# for some reason it does not work if this method is added to the reopened Kernel module
class Object
  def add_checked_attr(klass, name)
    eval("class #{klass}

            def #{name}=(value)
              raise ArgumentError unless value
              @#{name} = value
            end

            def #{name}
              @#{name}
            end
         end", nil, __FILE__, __LINE__ + 1 # pass the current file and the current line to eval for debugging
    )
  end
end

# in the first attempt, checked values accept everything except nil and false
class Person
end

describe Person do

  let(:bob) { Person.new }

  it 'should add accessor methods' do
    bob.should_not respond_to :age
    bob.should_not respond_to :age=

    add_checked_attr(Person, :age)

    bob.should respond_to :age
    bob.should respond_to :age=
  end

  it 'should accept valid values' do

    add_checked_attr(Person, :age)
    bob.age = 20
    bob.age.should be 20
  end

  it 'should reject nil values' do

    add_checked_attr(Person, :age)
    lambda { bob.age = nil }.should raise_error(ArgumentError)
  end

  it 'should reject false values' do
    add_checked_attr(Person, :age)
    lambda { bob.age = false }.should raise_error(ArgumentError)
  end
end
