def login(name, password, msg)
  p "name: #{name}"
  p "password: #{password}"
  p "msg: #{msg}"
end

login('my_secret_pw', 'myName', 'hi there!') # wrong order

# changing the signature, so order does not matter
# since we are passing a hash of named arguments
def new_login(args)
  p "name: #{args[:name]}"
  p "password: #{args[:password]}"
  p "msg: #{args[:msg]}"
end

# but it does destroy the method signature
new_login(password: 'my_secret_pw', name: 'myName', msg: 'hi there!')

# login(password: 'my_secret_pw', name: 'myName', msg: 'hi there!') does not work (1 argument instead of 3)

# since ruby 2 you can do

# the named arguments must have default values
# **options, is like a splat (*) for named args
def new_named_args(name: 'me, Mario', password: 'nevermore', **options)
  p '--- new named args since 2.0'
  p "name: #{name}"
  p "password: #{password}"
  p "options: #{options}"
end

# options do not have to come last
new_named_args( other: 'something', password: 'abra',   name: 'Don Quichote')

p 'look here: http://brainspec.com/blog/2012/10/08/keyword-arguments-ruby-2-0/'


