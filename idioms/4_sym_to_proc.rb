names = ['bob' , 'bill' , 'heather' ]

p names.map {|name| name.capitalize } # => ["Bob", "Bill", "Heather"]

p names.map(&:capitalize) # => ["Bob", "Bill", "Heather"]

# as if we implemented sth like this
#class Symbol
#  def to_proc
#    Proc.new {|x| x.send(self) }
#  end
#end


p [1, 2, 5].inject(0) {|memo, obj| memo + obj }

# with Symbol#to_proc:
p [1, 2, 5].inject(0, &:+)
# cool!
# => 8
