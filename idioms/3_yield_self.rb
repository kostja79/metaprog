# if tap werent implemented yet, you could do it like this

class Object
  def tap
    yield self if block_given?
    self
  end
end

p ['a' , 'b' , 'c' ].push('d' ).shift.tap {|x| puts x }.upcase.next # a "B"

# self yield enables us to do

spec = Gem::Specification.new do |s|
  s.name = "My Gem name"
  s.version = "0.0.1"
# ...
end

# module Gem
#  class Specification
#    def initialize
#      yield self if block_given?