# --- array arguments

# you can only have one arg array per method declaration
def array_args(*args)
  p args
end

array_args 'a', 'b', 'c'

def horrible(*args, **options)
  p "args: #{args}"
  p "opts: #{options}"
end

horrible( 'a', 'x', a: 'x', x: 'y')

# you cannot place options before arguments here
# horrible( x: 'y', 'a', 'x', a: 'x')
