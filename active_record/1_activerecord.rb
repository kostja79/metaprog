require 'active_record'

# TODO - how to work with activerecord without rails?

ActiveRecord::Base.establish_connection :adapter => "sqlite3",
                                        :database => ":memory:" # using an in-mem db

# 'migrating' the db
ActiveRecord::Base.connection.create_table :tasks do |t|
  t.string :name
  t.boolean :completed
end


class Task < ActiveRecord::Base
  validates_length_of :name, :maximum => 6
end

my_duck = Task.new

my_duck.name = "Donald Duck"
p my_duck.valid? # false


my_duck.name = "Donald"
my_duck.completed = true
p my_duck.valid? # true

my_duck.save

p Task.all

p Task.find(:first, :conditions => {:completed => true})

p Task.find_or_create_by_name_and_completed('Water', false)


