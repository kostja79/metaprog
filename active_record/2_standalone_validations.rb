require 'active_record'

# the book says the reference is needed (p.203), but I have not yet seen an error
# ActiveRecord::Base

class MyClass

  # the book says the save methods are needed for validation (p.203), but im not sure
  #def save
  #end
  #def save!
  #end

  def new_record? # needed, otherwise error
    true
  end

  include ActiveRecord::Validations
  attr_accessor :attr
  validates_length_of :attr, :minimum => 4

end

obj = MyClass.new

p obj.valid? # false
obj.attr = 'test'
p obj.valid? # true