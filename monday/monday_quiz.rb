class MondayQuiz


end

module Printable

  def print
    p 'Printable#print'
  end
end


module Document

  def print
    p 'Document#print'
  end

end

class Bookish
  include Document
  def print
   p 'Bookish#print'
  end
end

# if the class is not derived from another calss immportign the same module,
# you can influence the sequance of the ancestors by reprdering them

class Book1
  include Printable
  include Document

  def printme
    print
  end
end

# the Document module has been included in the parent class already, so it is not included again
# reordering will not change the fact that only Printable#print is being called

class Book2 < Bookish
  include Printable
  include Document

  def printme
    print
  end
end

require 'pry'

# start a REPL session
# binding.pry
# program resumes here (after pry session) puts "program resumes here."

b = Book1.new
b.printme

b = Book2.new
b.printme

p Book1.ancestors
p Book2.ancestors