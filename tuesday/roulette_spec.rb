require 'rspec'

class Roulette

  def method_missing(name, *args)
    capitalized = name.to_s.capitalize # unify different capitalizations
       # name guard for method acceptance
    super unless %w[Bob Bill Frank].include? capitalized
    r = 0 # needs to be initialized, otherwise, the call to r= will be interpreted as a call a missing method
    3.times do
      r = Random.rand(10) + 1
      p r
    end
    p "#{capitalized} got #{r}"
    r
  end

end

describe Roulette do

  let(:roulette) { Roulette.new }

  it 'should return a number' do
    roulette.bill.should be_a Fixnum
  end

  it 'result should vary' do
    bill = roulette.bill
    bill2 = roulette.bill
    bill.should_not be == bill2
  end
end