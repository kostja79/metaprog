require 'rspec'

class MyOpenStruct

  def initialize
    @attributes = {}
  end

  def method_missing(name, *args)
    if name =~ /=$/
      #convert to string, cut off the '=' and convert to symbol again
      @attributes[name.to_s.chop.to_sym] = args[0]
    else
      @attributes[name]
    end

  end
end

describe MyOpenStruct do

  let(:my_open_struct) { MyOpenStruct.new}

  it 'should return nil for uninitialized attribute' do
      my_open_struct.unknown_attribute.should be_nil
  end

  it 'should return the previously set value for initialized attribute' do
    my_open_struct.new_attribute = 123
    my_open_struct.new_attribute.should == 123
  end
end